//
//  ProductDetailBuilder.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//

import UIKit

class ProductDetailBuilder {

    static func make(model: Product) -> ProductDetailViewController {
        let storyBoard = UIStoryboard(name: "ProductList", bundle: nil)
        let view: ProductDetailViewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        let interactor = ProductDetailInteractor()
        let router = ProductDetailRouter(viewController: view)
        let presenter = ProductDetailPresenter(view: view, interactor: interactor, router: router)

        presenter.product = model
        view.presenter = presenter
        
        return view
    }
}
