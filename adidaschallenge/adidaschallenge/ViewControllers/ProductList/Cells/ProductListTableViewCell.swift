//
//  ProductListTableViewCell.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 31.10.2021.
//

import UIKit
import Kingfisher

class ProductListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bind(model: Product) {
        let imgURL = URL(string: model.imgUrl)
        imgView?.kf.indicatorType = .activity
        imgView?.kf.setImage(with: imgURL, options: [.transition(.fade(0.2))])

        productNameLabel.text = model.name
        descriptionLabel.text = model.description
        priceLabel.text = model.priceFormatted
    }
}
