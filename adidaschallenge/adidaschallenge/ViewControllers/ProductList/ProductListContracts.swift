//
//  ProductListContracts.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//

import Foundation

// MARK: Presenter
protocol ProductListPresenterProtocol: AnyObject {
    var view: ProductListViewProtocol? {get set}
    var interactor: ProductListInteractorProtocol? { get set}
    var router: ProductListRouterProtocol? { get set }
    func navigate(_ route: ProductListRoutes)

    func viewDidLoad()
    var displayList: [Product] { get set}
    func getProducts(by searchText: String, isOnline: Bool)
}

enum ProductListPresenterOutput {
    case reloadTable
    case setTitle(title: String)
}

// MARK: Interactor -
protocol ProductListInteractorProtocol: AnyObject {
    var delegate: ProductListInteractorDelegate? { get set }
    func getProducts()
}

protocol ProductListInteractorDelegate: AnyObject {
    func handleOutput(_ output: ProductListInteractorOutput)
}

enum ProductListInteractorOutput {
    case productReceived(list: [Product])
}

// MARK: View
protocol ProductListViewProtocol: AnyObject {
    var presenter: ProductListPresenterProtocol? { get set }
    func handleOutput(_ output: ProductListPresenterOutput)
}

// MARK: Router

enum ProductListRoutes {
    case detail(model: Product)
}

protocol ProductListRouterProtocol: AnyObject {
    func navigate(_ route: ProductListRoutes)
}
