//
//  ProductDetailInteractor.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import Moya

class ProductDetailInteractor {

    //MARK:- viper delegates
    weak var delegate: ProductDetailInteractorDelegate?
}

//MARK:- ProductDetailInteractorProtocol methods
extension  ProductDetailInteractor:  ProductDetailInteractorProtocol {
    func getReviews(productId: String) {
        let provider = MoyaProvider<ReviewService>()
        provider.request(.getReviews(productId: productId)) { result in
            switch result {
            case let .success(response):
            let statusCode = response.statusCode
                print("\n\n-----> RESPONSE: (\(ReviewService.getReviews(productId: productId).baseURL)\(ReviewService.getReviews(productId: productId).path)) Status Code: \(statusCode)")
                do {
                    _ = try response.filterSuccessfulStatusCodes()
                    print("\(prettyJSONString(data: response.data))")
                    let decoder = JSONDecoder()
                    let list = try decoder.decode([ProductReview].self, from: response.data)
                    self.delegate?.handleOutput(.reviewsReceived(list: list))

                } catch {
                    print("\n\n-----> RESPONSE: (\(ReviewService.getReviews(productId: productId).path)) Error: \(error.localizedDescription)")
                }
            case let .failure(error):
                print("\n\n-----> RESPONSE: (\(ReviewService.getReviews(productId: productId).path)) Error: \(error.errorDescription)")
            }
        }
    }
}
