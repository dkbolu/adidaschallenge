//
//  ProductListViewController.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class ProductListViewController: UIViewController {

    // MARK: - viper delegates
    var presenter: ProductListPresenterProtocol?

    // MARK: - ui controls
    @IBOutlet weak var tableView: UITableView!

    // MARK: - members
    var searchBar: UISearchBar?
    var rightButtons: [UIBarButtonItem]?
    var navTitle: String?
    var refreshControl = UIRefreshControl()

    // MARK: - initialize
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        prepareTableView()
        prepareSearchButton()
    }

    // MARK: - custom methods
    private func prepareTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()

        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)

        let nib = UINib(nibName: "\(ProductListTableViewCell.self)", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
    }

    @objc func refresh(_ sender: AnyObject) {
        if let searchBar = searchBar {
            searchBarCancelButtonClicked(searchBar)
        }
        presenter?.getProducts(by: "", isOnline: true)
    }

    private func prepareSearchButton() {
        let searchBarButton = createNavButton(image: "Search",
                                              width: 20, height: 20,
                                              target: self,
                                              action: #selector(prepareSearchBar))

        if navigationItem.rightBarButtonItems == nil {
            navigationItem.rightBarButtonItems = []
        }

        navigationItem.rightBarButtonItems?.append(searchBarButton)
    }

    private func createNavButton(image: String, width: Double, height: Double, target: Any?, action: Selector) -> UIBarButtonItem {
        let viewWidth = width + 8.0
        let viewHeight = 44.0
        let v = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))

        let imageX = viewWidth-width
        let imageY = (viewHeight-height)/2.0
        let imgv = UIImageView(frame: CGRect(x: imageX, y: imageY, width: width, height: height))
        imgv.image = UIImage(named: image)
        imgv.contentMode = UIView.ContentMode.scaleAspectFit
        v.addSubview(imgv)

        let button = UIButton(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        button.addTarget(target, action: action, for: UIControl.Event.touchUpInside)
        v.addSubview(button)

        let barButton = UIBarButtonItem(customView: v)
        return barButton
    }

    @objc func prepareSearchBar() {
        navigationItem.rightBarButtonItems = nil

        searchBar = UISearchBar()
        searchBar?.placeholder = "Search"
        searchBar?.delegate = self
        searchBar?.setShowsCancelButton(true, animated: true)
        definesPresentationContext = true
        var cancelButton: UIButton
        let topView: UIView = searchBar?.subviews[0] ?? UIView()
        for subView in (topView.subviews.last?.subviews)! where subView is UIButton {
            cancelButton = subView as! UIButton
            cancelButton.setTitle("Cancel", for: UIControl.State.normal)
        }

        navigationItem.titleView = searchBar
        searchBar?.becomeFirstResponder()
        searchBar?.delegate?.searchBarSearchButtonClicked!(searchBar!)
    }
}

// MARK: - ProductListViewProtocol methods
extension ProductListViewController: ProductListViewProtocol {
    
    func handleOutput(_ output:  ProductListPresenterOutput) {
        switch output {
        case .reloadTable:
            if refreshControl.isRefreshing {
                refreshControl.endRefreshing()
            }
            tableView.reloadData()
        case .setTitle(title: let title):
            self.navigationItem.title = title
        }
    }
}

//MARK: - TableView methods
extension ProductListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.displayList.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductListTableViewCell
        if let model = presenter?.displayList[indexPath.row] {
            cell.bind(model: model)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = presenter?.displayList[indexPath.row] {
            presenter?.navigate(.detail(model: model))
        }
    }
}

//MARK: - UISearchBarDelegate methods
extension ProductListViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.getProducts(by: searchText, isOnline: false)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationItem.titleView = nil
        prepareSearchButton()
        self.searchBar = nil
        presenter?.getProducts(by: "", isOnline: false)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

    }
}
