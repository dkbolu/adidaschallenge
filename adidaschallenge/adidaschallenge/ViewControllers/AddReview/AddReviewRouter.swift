//
//  AddReviewRouter.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class AddReviewRouter {
    unowned var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension AddReviewRouter: AddReviewRouterProtocol {
    func navigate(_ route: AddReviewRoutes) {
        switch route {
        }
    }
}
