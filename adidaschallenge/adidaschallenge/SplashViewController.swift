//
//  SplashViewController.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 31.10.2021.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let viewController = ProductListBuilder.make()
        let navCon = UINavigationController(rootViewController: viewController)
        navCon.navigationBar.tintColor = .black

        let window: UIWindow? = (UIApplication.shared.windows.first)!
        window?.rootViewController = navCon
        window?.makeKeyAndVisible()

//        UIView.transition(with: window!,
//                          duration: 0.3,
//                          options: UIView.AnimationOptions.transitionCrossDissolve,
//                          animations: nil,
//                          completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
