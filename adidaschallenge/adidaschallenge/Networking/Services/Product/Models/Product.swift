//
//  Product.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 31.10.2021.
//

import Foundation

class Product: Decodable {
    var id = ""
    var name = ""
    var description = ""
    var imgUrl = ""
    var currency = ""
    var price = 0.0
    var priceFormatted: String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2

        return "\(formatter.string(from: NSNumber(value: price)) ?? "0.00") \(currency)"
    }
}
