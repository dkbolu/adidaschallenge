//
//  ProductListPresenter.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class ProductListPresenter {
    
    //MARK:- viper delegates
    weak var view: ProductListViewProtocol?
    var interactor: ProductListInteractorProtocol?
    var router: ProductListRouterProtocol?

    //MARK: - members
    var productList = [Product]()
    var displayList = [Product]()

    //MARK:- initialize
    init(view: ProductListViewProtocol, interactor: ProductListInteractorProtocol, router: ProductListRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        self.interactor?.delegate = self
    }

}

//MARK:- ProductListPresenterProtocol methods
extension ProductListPresenter: ProductListPresenterProtocol {
    func viewDidLoad() {
        view?.handleOutput(.setTitle(title: "Products"))
        interactor?.getProducts()
    }
    
    func navigate(_ route: ProductListRoutes) {
        router?.navigate(route)
    }

    func getProducts(by searchText: String, isOnline: Bool) {
        if isOnline {
            interactor?.getProducts()
        } else {
            if searchText.isEmpty {
                displayList = productList
            } else {
                displayList = productList.filter { item in
                    item.name.lowercased().contains(searchText.lowercased()) || item.description.lowercased().contains(searchText.lowercased())
                }
            }
            view?.handleOutput(.reloadTable)
        }
    }
    
}

//MARK:- ProductListInteractorDelegate methods
extension ProductListPresenter: ProductListInteractorDelegate {
    func handleOutput(_ output: ProductListInteractorOutput) {
        switch output {
        case .productReceived(let list):
            productList = list
            displayList = list
            view?.handleOutput(.reloadTable)
        }
    }
}

