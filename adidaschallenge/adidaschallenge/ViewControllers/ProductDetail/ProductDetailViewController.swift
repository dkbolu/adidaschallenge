//
//  ProductDetailViewController.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class ProductDetailViewController: UIViewController {

    // MARK: - viper delegates
    var presenter: ProductDetailPresenterProtocol?

    // MARK: - ui controls
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addReviewButton: UIButton!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - members

    // MARK: - initialize
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        prepareTableView()
    }

    // MARK: - custom methods
    private func prepareTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()

        let nib = UINib(nibName: "\(RatingTableViewCell.self)", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ratingCell")
    }

    @IBAction func addReviewButtonTapped(_ sender: Any) {
        presenter?.navigateAddReview()
    }
}

//MARK:- ProductDetailViewProtocol methods
extension ProductDetailViewController: ProductDetailViewProtocol {
    
    func handleOutput(_ output:  ProductDetailPresenterOutput) {
        switch output {
        case .setTitle(let title):
            navigationItem.title = title
        case .displayProduct(let model):
            let imgURL = URL(string: model.imgUrl)
            imgView?.kf.indicatorType = .activity
            imgView?.kf.setImage(with: imgURL, options: [.transition(.fade(0.2))])

            productNameLabel.text = model.name
            descriptionLabel.text = model.description
            priceLabel.text = model.priceFormatted
        case .reloadTable:
            tableView.reloadData()
        }
    }
}

//MARK: - TableView methods
extension ProductDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.reviews.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ratingCell", for: indexPath) as! RatingTableViewCell
        if let model = presenter?.reviews[indexPath.row] {
            cell.bind(model: model)
        }
        return cell
    }
}

//MARK: - AddReviewDelegate
extension ProductDetailViewController: AddReviewDelegate {
    func ratingPosted(willReload: Bool) {
        if willReload {
            presenter?.getReviews()
        }
    }
}
