//
//  AddReviewContracts.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//

import Foundation

// MARK: Presenter
protocol AddReviewPresenterProtocol: AnyObject {
    var view: AddReviewViewProtocol? {get set}
    var interactor: AddReviewInteractorProtocol? { get set}
    var router: AddReviewRouterProtocol? { get set }
    func navigate(_ route: AddReviewRoutes)

    func viewDidLoad()
    func postReview(text: String, rating: Int)
}

enum AddReviewPresenterOutput {
    case setTitle(title: String)
    case dissmis
}

// MARK: Interactor -
protocol AddReviewInteractorProtocol: AnyObject {
    var delegate: AddReviewInteractorDelegate? { get set }
    func postReview(model: ProductReview)
}

protocol AddReviewInteractorDelegate: AnyObject {
    func handleOutput(_ output: AddReviewInteractorOutput)
}

enum AddReviewInteractorOutput {
    case reviewPosted
}

// MARK: View
protocol AddReviewViewProtocol: AnyObject {
    var presenter: AddReviewPresenterProtocol? { get set }
    func handleOutput(_ output: AddReviewPresenterOutput)
}

// MARK: Router

enum AddReviewRoutes {
    
}

protocol AddReviewRouterProtocol: AnyObject {
    func navigate(_ route: AddReviewRoutes)
}
