//
//  ProductListBuilder.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//

import UIKit

class ProductListBuilder {
    static func make() -> ProductListViewController {
        let storyBoard = UIStoryboard(name: "ProductList", bundle: nil)
        let view: ProductListViewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        let interactor = ProductListInteractor()
        let router = ProductListRouter(viewController: view)
        let presenter = ProductListPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
}
