//
//  ProductDetailPresenter.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class ProductDetailPresenter {
    
    //MARK:- viper delegates
    weak var view: ProductDetailViewProtocol?
    var interactor: ProductDetailInteractorProtocol?
    var router: ProductDetailRouterProtocol?

    //MARK: - members
    var product: Product!
    var reviews = [ProductReview]()

    //MARK:- initialize
    init(view: ProductDetailViewProtocol, interactor: ProductDetailInteractorProtocol, router: ProductDetailRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        self.interactor?.delegate = self
    }
}

//MARK:- ProductDetailPresenterProtocol methods
extension ProductDetailPresenter: ProductDetailPresenterProtocol {
    func viewDidLoad() {
        view?.handleOutput(.setTitle(title: product.name))
        view?.handleOutput(.displayProduct(model: product))

        getReviews()
    }

    func getReviews() {
        interactor?.getReviews(productId: product.id)
    }

    func navigateAddReview() {
        navigate(.addReview(productId: product.id))
    }

    func navigate(_ route: ProductDetailRoutes) {
        router?.navigate(route)
    }
}

//MARK:- ProductDetailInteractorDelegate methods
extension ProductDetailPresenter: ProductDetailInteractorDelegate {
    func handleOutput(_ output: ProductDetailInteractorOutput) {
        switch output {
        case .reviewsReceived(list: let list):
            reviews = list
            view?.handleOutput(.reloadTable)
        }
    }
}

