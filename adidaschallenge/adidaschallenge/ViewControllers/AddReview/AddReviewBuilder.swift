//
//  AddReviewBuilder.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//

import UIKit

class AddReviewBuilder {
    static func make(productId: String, delegate: AddReviewDelegate?) -> AddReviewViewController {
        let storyBoard = UIStoryboard(name: "ProductList", bundle: nil)
        let view: AddReviewViewController = storyBoard.instantiateViewController(withIdentifier: "AddReviewViewController") as! AddReviewViewController
        let interactor = AddReviewInteractor()
        let router = AddReviewRouter(viewController: view)
        let presenter = AddReviewPresenter(view: view, interactor: interactor, router: router)

        presenter.productId = productId
        view.delegate = delegate
        view.presenter = presenter
        
        return view
    }
}
