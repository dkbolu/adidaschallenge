//
//  ProductDetailContracts.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//

import Foundation

// MARK: Presenter
protocol ProductDetailPresenterProtocol: AnyObject {
    var view: ProductDetailViewProtocol? {get set}
    var interactor: ProductDetailInteractorProtocol? { get set}
    var router: ProductDetailRouterProtocol? { get set }
    func navigate(_ route: ProductDetailRoutes)

    func viewDidLoad()
    var reviews: [ProductReview] { get set }
    func navigateAddReview()
    func getReviews()
}

enum ProductDetailPresenterOutput {
    case setTitle(title: String)
    case displayProduct(model: Product)
    case reloadTable
}

// MARK: Interactor -
protocol ProductDetailInteractorProtocol: AnyObject {
    var delegate: ProductDetailInteractorDelegate? { get set }
    func getReviews(productId: String)
}

protocol ProductDetailInteractorDelegate: AnyObject {
    func handleOutput(_ output: ProductDetailInteractorOutput)
}

enum ProductDetailInteractorOutput {
    case reviewsReceived(list: [ProductReview])
}

// MARK: View
protocol ProductDetailViewProtocol: AnyObject {
    var presenter: ProductDetailPresenterProtocol? { get set }
    func handleOutput(_ output: ProductDetailPresenterOutput)
}

// MARK: Router

enum ProductDetailRoutes {
    case addReview(productId: String)
}

protocol ProductDetailRouterProtocol: AnyObject {
    func navigate(_ route: ProductDetailRoutes)
}
