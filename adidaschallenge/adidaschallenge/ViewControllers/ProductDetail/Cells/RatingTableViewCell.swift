//
//  RatingTableViewCell.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import UIKit

class RatingTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingTextLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bind(model: ProductReview) {
        view.layer.cornerRadius = 20
        ratingTextLabel.text = model.text
        ratingLabel.text = "\(model.rating)"
    }
}
