//
//  ReviewService.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 31.10.2021.
//

import Moya

public enum ReviewService {
    case getReviews(productId: String)
    case postReview(model: ProductReview)
}

extension ReviewService: TargetType {

    public var baseURL: URL { return URL(string: "http://localhost:3002/")!}

    public var path: String {
        switch self {
        case .getReviews(let productId):
            return "reviews/\(productId)"
        case .postReview(let review):
            return "reviews/\(review.productId)"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .getReviews:
            return .get
        case .postReview:
            return .post
        }
    }

    public var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }

    // MARK: - Requests
    public var task: Task {
        switch self {
        case .getReviews:
            return .requestPlain
        case .postReview(let model):
            return .requestJSONEncodable(model)
        }
    }

    public var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

