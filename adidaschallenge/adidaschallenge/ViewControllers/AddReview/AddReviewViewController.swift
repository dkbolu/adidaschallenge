//
//  AddReviewViewController.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

protocol AddReviewDelegate: AnyObject {
    func ratingPosted(willReload: Bool)
}

class AddReviewViewController: UIViewController {

    // MARK: - viper delegates
    var presenter: AddReviewPresenterProtocol?

    // MARK: - ui controls

    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var starStackView: UIStackView!

    // MARK: - members
    weak var delegate: AddReviewDelegate?
    var rating = 0

    // MARK: - initialize
	override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(handleSingleTap(recognizer:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

        reviewTextView.layer.borderWidth = 1
        reviewTextView.layer.borderColor = UIColor.black.cgColor
    }

    // MARK: - custom methods
    @IBAction func sendReviewButtonTapped(_ sender: Any) {
        if let text  = reviewTextView.text, rating > 0 {
            presenter?.postReview(text: text, rating: rating)
        }
    }

    @IBAction func closeButtonTapped(_ sender: Any) {
        if let delegate = delegate {
            delegate.ratingPosted(willReload: false)
        }
        dismiss(animated: true, completion: nil)
    }

    @objc func handleSingleTap(recognizer: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBAction func starTapped(_ sender: UIButton) {
        for view in starStackView.subviews where view is UIButton{
            if view.tag <= sender.tag {
                (view as! UIButton).setBackgroundImage(UIImage(systemName: "star.fill"), for: .normal)
            } else {
                (view as! UIButton).setBackgroundImage(UIImage(systemName: "star"), for: .normal)
            }
        }
        rating = sender.tag
    }
}

// MARK: - AddReviewViewProtocol methods
extension AddReviewViewController: AddReviewViewProtocol {
    
    func handleOutput(_ output:  AddReviewPresenterOutput) {
        switch output {
        case .setTitle(title: let title):
            navigationItem.title = title
        case .dissmis:
            if let delegate = delegate {
                delegate.ratingPosted(willReload: true)
            }
            dismiss(animated: true, completion: nil)
        }
    }
}
