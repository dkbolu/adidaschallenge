//
//  AddReviewPresenter.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class AddReviewPresenter {
    
    //MARK:- viper delegates
    weak var view: AddReviewViewProtocol?
    var interactor: AddReviewInteractorProtocol?
    var router: AddReviewRouterProtocol?

    //MARK:- members
    var productId: String!

    //MARK:- initialize
    init(view: AddReviewViewProtocol, interactor: AddReviewInteractorProtocol, router: AddReviewRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        self.interactor?.delegate = self
    }

}

//MARK:- AddReviewPresenterProtocol methods
extension AddReviewPresenter: AddReviewPresenterProtocol {
    func postReview(text: String, rating: Int) {
        let model = ProductReview()
        model.productId = productId
        model.locale = "tr-tr"
        model.rating = rating
        model.text = text

        interactor?.postReview(model: model)
    }

    func viewDidLoad() {
        view?.handleOutput(.setTitle(title: "Add Review"))
    }
    
    func navigate(_ route: AddReviewRoutes) {
        router?.navigate(route)
    }
    
}

//MARK:- AddReviewInteractorDelegate methods
extension AddReviewPresenter: AddReviewInteractorDelegate {
    func handleOutput(_ output: AddReviewInteractorOutput) {
        switch output {
        case .reviewPosted:
            view?.handleOutput(.dissmis)
        }
    }
}

