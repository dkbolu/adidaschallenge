//
//  ProductListRouter.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class ProductListRouter {
    unowned var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension ProductListRouter: ProductListRouterProtocol {
    func navigate(_ route: ProductListRoutes) {
        switch route {
        case .detail(model: let model):
            let vc = ProductDetailBuilder.make(model: model)
            viewController.show(vc, sender: nil)
        }
    }
}
