//
//  AddReviewInteractor.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import Moya

class AddReviewInteractor {

    //MARK:- viper delegates
    weak var delegate: AddReviewInteractorDelegate?
}

//MARK:- AddReviewInteractorProtocol methods
extension  AddReviewInteractor:  AddReviewInteractorProtocol {
    func postReview(model: ProductReview) {
        let provider = MoyaProvider<ReviewService>()
        provider.request(.postReview(model: model)) { result in
            switch result {
            case let .success(response):
            let statusCode = response.statusCode
                print("\n\n-----> RESPONSE: (\(ReviewService.postReview(model: model).path)) Status Code: \(statusCode)")
                do {
                    _ = try response.filterSuccessfulStatusCodes()
                    print("\(prettyJSONString(data: response.data))")
                    self.delegate?.handleOutput(.reviewPosted)

                } catch {
                    print("\n\n-----> RESPONSE: (\(ReviewService.postReview(model: model).path)) Error: \(error.localizedDescription)")
                }
            case let .failure(error):
                print("\n\n-----> RESPONSE: (\(ReviewService.postReview(model: model).path)) Error: \(error.errorDescription)")
            }
        }
    }

    
}
