//
//  ProductService.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 31.10.2021.
//

import Moya

public enum ProductService {
    case getProducts
}

extension ProductService: TargetType {

    public var baseURL: URL { return URL(string: "http://localhost:3001/")!}

    public var path: String {
        switch self {
        case .getProducts:
            return "product"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .getProducts:
            return .get
        }
    }

    public var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }

    // MARK: - Requests
    public var task: Task {
        switch self {
        case .getProducts:
            return .requestParameters(parameters: [:], encoding: URLEncoding.queryString)
        }
    }

    public var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

