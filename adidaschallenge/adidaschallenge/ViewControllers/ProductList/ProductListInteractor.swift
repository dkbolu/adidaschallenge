//
//  ProductListInteractor.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import Moya

class ProductListInteractor {

    //MARK:- viper delegates
    weak var delegate: ProductListInteractorDelegate?
}

//MARK:- ProductListInteractorProtocol methods
extension  ProductListInteractor:  ProductListInteractorProtocol {
    func getProducts() {
        let provider = MoyaProvider<ProductService>()
        provider.request(.getProducts) { result in
            switch result {
            case let .success(response):
            let statusCode = response.statusCode
                print("\n\n-----> RESPONSE: (\(ProductService.getProducts.path)) Status Code: \(statusCode)")
                do {
                    _ = try response.filterSuccessfulStatusCodes()
//                    let header = response.response?.allHeaderFields
                    print("\(prettyJSONString(data: response.data))")
                    let decoder = JSONDecoder()
                    let list = try decoder.decode([Product].self, from: response.data)
                    self.delegate?.handleOutput(.productReceived(list: list))

                } catch {
                    print("\n\n-----> RESPONSE: (\(ProductService.getProducts.path)) Error: \(error.localizedDescription)")
                }
            case let .failure(error):
                print("\n\n-----> RESPONSE: (\(ProductService.getProducts.path)) Error: \(error.errorDescription)")
            }
        }

    }
}

func prettyJSONString(data: Data) -> String {
    guard let object = try? JSONSerialization.jsonObject(with: data, options: []),
        let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
        let prettyPrintedString = String(data: data, encoding: .utf8) else { return "" }

    return prettyPrintedString
}
