//
//  ProductReview.swift
//  adidaschallenge
//
//  Created by Doruk Kaan Bolu on 31.10.2021.
//

import Foundation

public class ProductReview: Codable {
    var productId = ""
    var locale = ""
    var rating = 0
    var text = ""

}
