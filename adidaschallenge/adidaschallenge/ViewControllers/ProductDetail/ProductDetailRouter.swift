//
//  ProductDetailRouter.swift
//  adidaschallenge
//
//  Created Doruk Kaan Bolu on 31.10.2021.
//  Copyright © 2021 dkbolu. All rights reserved.
//


import UIKit

class ProductDetailRouter {
    unowned var viewController: UIViewController

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension ProductDetailRouter: ProductDetailRouterProtocol {
    func navigate(_ route: ProductDetailRoutes) {
        switch route {
        case .addReview(let productId):
            let vc = AddReviewBuilder.make(productId: productId, delegate: viewController as? AddReviewDelegate)

            vc.modalPresentationStyle = .overFullScreen
            viewController.present(vc, animated: true, completion: nil)
        }
    }
}
